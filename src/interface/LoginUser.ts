export class LoginUser {
    id?:number;
    str_login_user: string = "";
    str_login_password:string = "";
    str_login_token:string = "";
    id_type_access:string = "";

    constructor(id: number, str_login_user: string, str_login_password: string, str_login_token: string, id_type_access: string){
        this.id = id;
        this.str_login_user = str_login_user;
        this.str_login_password = str_login_password;
        this.str_login_token = str_login_token;
        this.id_type_access = id_type_access;
    }

}