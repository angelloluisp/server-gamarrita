export interface Shop {
    id?:number;
    str_shop_name:string;
    str_shop_address:string;
    id_seller:number;
    str_shop_latitude:string;
    str_shop_longitude:string;
    str_shop_cellphone:string;
}