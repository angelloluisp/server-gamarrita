
export interface User {
    id?: number;
    str_user_name:string;
    str_user_mom_lastname:string;
    str_user_father_lastname:string;
    str_user_dni:string;
    str_user_cell:string;
    login_id:number;
}