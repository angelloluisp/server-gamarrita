import { Router } from "express";
//const router = Router();

import { ShopController } from "../controllers/shop.controller";

export class ShopRoutes {

    public router:Router;
    public shopController: ShopController = new ShopController();

    constructor(){
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.get('/', this.shopController.getShops);
        this.router.get('/:id', this.shopController.getShop);
        this.router.post('/', this.shopController.createShop);
        this.router.delete('/:id', this.shopController.deleteShop);
        this.router.put('/:id', this.shopController.updateShop);
    }

}

