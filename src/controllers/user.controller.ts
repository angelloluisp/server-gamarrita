import { Request, Response } from "express";
import bcrypt, { hash } from "bcrypt";
import { LoginUser } from "../interface/LoginUser";
import { connect } from "../database";

export class UserController {

    public async createUser(req: Request, res: Response): Promise<void>{
        const conn = await connect();
        console.log("aversh", req.body);
        bcrypt.hash(req.body.str_login_password, 10, (err, hash) => {
            if(err){
                return res.status(500).json({
                    error: err
                });
            }else{
                const loginUser = new LoginUser(
                    req.body.id,
                    req.body.str_login_user,
                    hash,
                    req.body.str_login_token,
                    req.body.id_type_access
                );
                try {
                    const formUser = conn.query(`INSERT INTO login_user SET ?`, [loginUser]);
                    console.log(formUser)
                    return res.status(201).json({
                        message: "User Created"
                    });
                } catch (error) {
                    res.status(500).json({
                        message: error
                    });
                }
            }
        });

    }

}