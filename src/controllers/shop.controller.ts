import { Request, Response } from "express";

import { connect } from "../database";
import { Shop } from "../interface/shop";

export class ShopController {

    public async getShops(req: Request, res: Response): Promise<Response>{
        const conn = await connect();
        const shops = await conn.query('select * from shop');
        return res.status(200).json(shops[0]);
    }

    public async getShop(req: Request, res: Response): Promise<Response>{
        const id = req.params.id;
        const conn = await connect();
        const shop  = await conn.query("SELECT * FROM shop WHERE id = ?", [id]);
        return res.status(200).json(shop[0]);
    }

    public async createShop(req: Request, res: Response): Promise<Response>{
        const newShop: Shop = req.body;
        const conn = await connect();
        await conn.query("INSERT INTO shop SET ?", [newShop]);
        return res.status(200).json({
            message: "Post created"
        });
    }

    public async deleteShop(req: Request, res: Response): Promise<Response>{
        const id = req.params.id;
        const conn = await connect();
        await conn.query("DELETE FROM shop WHERE id = ?", [id]);
        return res.status(200).json({
            message: "Shop deleted"
        });
    }
    
    public async updateShop(req: Request, res: Response): Promise<Response>{
        const id = req.params.id;
        const updateShop: Shop = req.body;
        const conn = await connect();
        await conn.query("UPDATE shop SET ? WHERE id = ?", [updateShop, id]);
        return res.status(200).json({
            message: "Shop updated"
        });
    }

}

